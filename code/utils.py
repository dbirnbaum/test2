from collections import defaultdict
from Bio.Seq import Seq
import random
import colorsys
import pandas as pd
import sys

def generate_hex_colors(n):
    random.seed(2)
    r = lambda: random.randint(100,255)
    colors = ['#%02X%02X%02X' % (r(),r(),r()) for i in range(n)]
    return colors

def rc(seq):
    return str(Seq(seq).reverse_complement())

def generate_combinations(combined, combined_name, previous, j, l, constants):
    df, feature_names, basis_dict = constants
    column_names = list(df.columns)
    if j == len(column_names):
        combined_seq = ''.join(combined)
        name = '_'.join(combined_name)
        part_len = [len(part) for part in combined]
        return [(combined_seq, name, part_len)]
    previous_column_names = [x[0] for x in previous]
    column = column_names[j] 
    idx = ~df.iloc[:,j].isna()
    seqs = list(df.loc[idx,].iloc[:,j].str.upper())
    n_current = len(seqs)
    if column in previous_column_names:
        i_prev = [x[1] for x in previous if x[0] == column]
        basis = basis_dict[column]
        mod_prev = [(i % basis) for i in i_prev]
        if len(set(mod_prev)) > 1:
            sys.exit('ERROR: previous columns in a given branch should all have the same basis')
        if len(mod_prev) == 0:
            # if this is the first occurence of the column in the recursive branch, use every row
            i_use = range(n_current)
        else:
            basis_use = mod_prev[0]
            i_use = [i for i in range(n_current) if (i % basis) == basis_use]
    else:
        i_use = range(n_current)
    for i in i_use:
        seq = seqs[i]
        name = feature_names[seq]
        l += generate_combinations(combined + [seq], combined_name + [name], previous + [(column, i)], j + 1, [], constants)
    return l

def get_basis(df):
    column_names = list(df.columns)
    d = defaultdict(list) # holds the length of each instance of each repeated column
    encountered = []
    for column in column_names:
        count = column_names.count(column)
        if count > 1 and column not in encountered:
            for j in range(count):
                l = sum(~df[column].iloc[:,j].isna())
                d[column].append(l)
        encountered.append(column)
    basis = defaultdict(lambda: float('inf'))
    for column in d:
        for col_len in d[column]:
            if col_len < basis[column]:
                basis[column] = col_len
    for column in d:
        # for now, enforce Nate's simplified behavior where any "zipped" columns must have equal length
        if len(set(d[column])) != 1:
            sys.exit('ERROR: repeat columns must have equal length - check %s' % column)
        for col_len in d[column]:
            if (col_len % basis[column]) != 0:
                sys.exit('ERROR: repeat columns must have divisible lengths - check %s' % column)
    return basis

def parse_feature_file(feature_file):
    ext = feature_file.split('.')[-1]
    if ext == 'xlsx':
        feature_df = pd.read_excel(feature_file)
    elif ext == 'csv':
        feature_df = pd.read_csv(feature_file)
    feature_df = feature_df.loc[~feature_df['feature_name'].isna(),]
    names = list(feature_df['feature_name'])
    seqs = list(feature_df['feature_sequence'].str.upper())
    supp = list((feature_df['supplemental'] * 1) == 1)
    if sum(' ' in name for name in names) > 0:
        print('WARNING: removing whitespace from feature names')
        names = [name.replace(' ', '') for name in names]
    if sum('_' in name for name in names) > 0:
        print('WARNING: converting underscores to hyphens for feature names')
        names = [name.replace('_', '-') for name in names]
    name2seq = dict(zip(names, seqs)) # also get reverse mapping
    seq2name = dict(zip(seqs, names))
    name2supp = dict(zip(names, supp))
    nrow = feature_df.shape[0]
    if len(list(seq2name.keys())) != nrow:
        sys.exit('ERROR: There are one or more duplicates in the feature_sequence column of feature_file')
    if len(list(name2seq.keys())) != nrow:
        seen = set()
        dupes = [x for x in names if x in seen or seen.add(x)]  
        sys.exit('ERROR: There are one or more duplicates in the feature_name column of feature_file: %s' % ','.join(dupes))
    return name2seq, seq2name, name2supp


def parse_combo_file(combo_file):
    ext = combo_file.split('.')[-1]
    if ext == 'xlsx':
        df = pd.read_excel(combo_file)
    elif ext == 'csv':
        df = pd.read_csv(combo_file)
    # check that file is formated appropriately
    df.columns = df.columns.str.split('.').str[0]
    bb_plasmid = df['backbone_plasmid'][0].upper()
    region = df['region_to_replace'][0].upper()
    if sum(~df['backbone_plasmid'][1:].isna()) > 0 or sum(~df['region_to_replace'][1:].isna()):
        sys.exit('ERROR: can only provide one backbone_plasmid and one region_to_replace per request')
    columns = list(df.columns)
    if tuple(columns[:2]) != ('backbone_plasmid', 'region_to_replace'):
        sys.exit('ERROR: first 2 columns must contain backbone_plasmid and region_to_replace')
    if bb_plasmid.count(region) != 1:
        sys.exit('ERROR: region_to_replace must occur once in backbone_plasmid')
    df = df.iloc[:,2:]
    column_names = list(df.columns)
    if sum('_' in name for name in column_names) > 0:
        sys.exit('ERROR: column names in combo_file may NOT contain \'_\'')
    if sum(' ' in name for name in column_names) > 0:
        sys.exit('ERROR: column names in combo_file may NOT contain any whitespace')
    return df, bb_plasmid, region



